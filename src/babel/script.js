$(() => {
    const initializeSlider = () => {
        $('.projects__slider__inner').slick({
            arrows: false,
            dots: true,
            appendDots: $('.project__slider__dots'),
            fade: true
        })
    }

    const handleFooter = () => {
        const buttons = document.querySelectorAll('.footer__row-mobile .footer__list__title')
        $('.footer__row-mobile .footer__list').hide()
        for (let b of buttons) {
            b.onclick = () => {
                const list = $(b).next('.footer__list')
                if (list.css('display') === 'none') {
                    list.show(400)
                } else {
                    list.hide(400)
                }
            }
        }
    }

    const initializeTabs = () => {
        const tabs = document.getElementById('tabs')
        const projects = document.getElementById('projects')
        new Waypoint({
            element: tabs,
            handler: function (dir) {
                dir === 'down' ? tabs.classList.add('tabs-fixed') : tabs.classList.remove('tabs-fixed')
            },
            offset: 0
        })
        new Waypoint({
            element: projects,
            handler: function (dir) {
                dir === 'up' ? tabs.classList.add('tabs-fixed') : tabs.classList.remove('tabs-fixed')
            },
            offset: 0
        })
    }

    const handleScroll = () => {
        const scroll = (elem) => {
            //elem - объект, который вызывает скролл, а не объект, до которого идет скролл. У elem должен быть установлен href.
            var goto = $(elem).attr('data-scroll')
            if (goto.length != 0) {
                $('html, body').animate({
                    scrollTop: $(goto).offset().top
                }, 500)
            }
            return false
        }

        $('.tabview__tablist__tab').click((e) => {
            // e.preventDefault()
            scroll(e.currentTarget)
        })
    }

    const handleHeader = () => {
        const buttons = document.querySelectorAll('.header__list:not(.header__list--mobile) .header__link')
        const menus = document.querySelectorAll('.header__drop')
        const mobileButtons = document.querySelectorAll('.header__list.header__list--mobile .header__link')
        const mobile = document.querySelector('.header__list--mobile')
        const ham = document.querySelector('.header__ham')

        for (let m of menus) {
            m.onblur = () => {
                m.style.display = 'none'
            }
        }

        for (let b of buttons) {
            b.onmouseover = (e) => {
                const menu = e.currentTarget.getAttribute('data-menu') && document.querySelector(e.currentTarget.getAttribute('data-menu'))

                if (menu !== false && menu !== undefined) {
                    menu.style.display = 'block'
                    menu.focus()
                }
            }
        }

        for (let b of mobileButtons) {
            b.onclick = (e) => {
                const menu = e.currentTarget.getAttribute('data-menu') && document.querySelector(e.currentTarget.getAttribute('data-menu'))

                if (menu !== false && menu !== null) {
                    if (menu.style.display === 'none' || menu.style.display === '') {
                        e.currentTarget.classList.add('active')
                    } else {
                        e.currentTarget.classList.remove('active')
                    }
                    
                    menu.style.display = (menu.style.display === 'none' || menu.style.display === '') ? 'block' : 'none'
                }
            }
        }

        ham.onclick = () => {
            mobile.setAttribute('data-shown', mobile.getAttribute('data-shown') !== 'true' ? 'true' : 'false')
        }

    }

    initializeSlider()
    handleFooter()
    initializeTabs()
    handleScroll()
    handleHeader()
})